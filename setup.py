from setuptools import setup

setup(
    name="srulik",
    packages=['srulik'],
    entry_points={
        'console_scripts': [
            'srulik = srulik.__main__:main',
        ],
    },
)
