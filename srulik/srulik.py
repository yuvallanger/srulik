#!/usr/bin/env python3

import logging
import ssl
import urllib
from functools import partial

import requests

from irc.client import ip_numstr_to_quad
import irc.bot
import irc.strings
import irc.connection
from typing import Any, List
from irc.client import Event
from irc.client import ServerConnection


logging.basicConfig(level='DEBUG')


def translate(text: str) -> List:
    logger = logging.getLogger(__name__)
    quoted_text = urllib.parse.quote_plus(text)
    # try:
    response = requests.get(
        'https://translate.googleapis.com/translate_a/single'
        '?client=gtx'
        '&sl=auto'
        '&tl=en'
        '&dt=t'
        '&q=' + quoted_text,
        headers=translate_request_headers,
    )
    logger.debug('response: %s', str(response))
    logger.debug('response headers: %s', str(response.headers))
    result = response.json()
    logger.debug('json(): %s', result)
    logger.debug('json() type: %s', str(type(result)))
    # except Exception as e:
    #     logger.exception(e, exc_info=True)
    #     result = 'Translation failed.'
    return result


class SrulikBot(irc.bot.SingleServerIRCBot):
    def __init__(
        self,
        channel: str,
        nickname: str,
        server: str,
        port: int=6697,
        **kwargs: Any,
    ) -> None:
        logger = logging.getLogger(__name__)
        super().__init__([(server, port)], nickname, nickname, **kwargs)
        logger.debug("channel: %s", channel)
        logger.debug("nickname: %s", nickname)
        logger.debug("server: %s", server)
        logger.debug("port: %s", port)
        self.channel = channel

    def on_nicknameinuse(self, c, e):
        logger = logging.getLogger(__name__)
        logger.debug("c: %s", c)
        logger.debug("e: %s", e)
        c.nick(c.get_nickname() + '_')

    def on_welcome(
        self,
        c: ServerConnection,
        e: Event,
    ) -> None:
        logger = logging.getLogger(__name__)
        logger.debug("c: %s", c)
        logger.debug("e: %s", e)

        self.reactor.scheduler.execute_after(
            5,
            partial(
                c.privmsg,
                'nickserv',
                f'identify {secret_server_password}',
            ),
        )
        self.reactor.scheduler.execute_after(
            10,
            partial(
                c.join,
                self.channel,
            ),
        )

    def parse_command(self, possible_command_line):
        possible_bot_name = possible_command_line.split(':', 1)[0]
        if (
            irc.strings.lower(possible_bot_name)
            == irc.strings.lower(self.connection.get_nickname())
        ):
            possible_command_and_argument = (
                possible_command_line
                .split(':', 1)[1]
                .strip()
            )
        possible_command = (
            possible_command_and_argument
            .split(' ', 1)[0]
            .strip()
        )
        possible_argument = (
            possible_command_and_argument
            .split(' ', 1)[1]
            .strip()
        )
        return possible_command, possible_argument

    def on_privmsg(
        self,
        c: ServerConnection,
        e: Event,
    ) -> None:
        logger = logging.getLogger(__name__)
        logger.debug("c: %s", c)
        logger.debug("e: %s", e)
        message = e.arguments[0]
        split_message = [n.strip() for n in message.split(' ', 1)]
        if len(split_message) > 1:
            command, argument = split_message
            self.do_command(e, command, argument)
        else:
            command = split_message[0]
            self.do_command(e, command)

    def on_pubmsg(
        self,
        c: ServerConnection,
        e: Event,
    ) -> None:
        logger = logging.getLogger(__name__)
        logger.debug("c: %s", c)
        logger.debug("e: %s", e)

        split_message = [n.strip() for n in e.arguments[0].split(':', 1)]
        if (
            len(split_message) > 1
            and irc.strings.lower(split_message[0])
            == irc.strings.lower(self.connection.get_nickname())
        ):
            _, command_line = split_message
            split_command_line = [
                n.strip() for n in command_line.split(' ', 1)]
            if len(split_command_line) > 1:
                command, argument = split_command_line
                self.do_command(e, command, argument)
            else:
                command = split_command_line[0]
                self.do_command(e, command)
        return

    def on_dccmsg(
        self,
        c: ServerConnection,
        e: Event,
    ) -> None:
        logger = logging.getLogger(__name__)
        logger.debug("c: %s", c)
        logger.debug("e: %s", e)
        text = e.arguments[0].decode('utf-8')
        c.privmsg('You said: ' + text)

    def on_dccchat(
        self,
        c: ServerConnection,
        e: Event,
    ) -> None:
        logger = logging.getLogger(__name__)
        logger.debug("c: %s", c)
        logger.debug("e: %s", e)
        if len(e.arguments) != 2:
            return
        args = e.arguments[1].split()
        if len(args) == 4:
            try:
                address = ip_numstr_to_quad(args[2])
                port = int(args[3])
            except ValueError:
                return
            self.dcc_connect(address, port)

    def do_command(
        self,
        e: Event,
        cmd: str,
        argument: str=None,
    ) -> None:
        logger = logging.getLogger(__name__)
        logger.debug('e: %s', e)
        logger.debug('cmd: %s', cmd)

        if (
            irc.strings.lower(e.target)
            == irc.strings.lower(self.connection.get_nickname())
        ):
            logger.debug("e.target: %s", e.target)
            logger.debug("my nickname: %s", self.connection.get_nickname())
            logger.debug("e.source.nick: %s", e.source.nick)
            nick = e.source.nick
        elif e.target == self.channel:
            nick = '#israel'

        connection = self.connection

        if cmd == "translate":
            self.translate(nick, argument)
        elif cmd == 'disconnect':
            self.disconnect(),
        elif cmd == 'die':
            raise SrulikDieException
        else:
            self.default_command(connection, nick, cmd, argument)

    def translate(self, nick, text):
        logger = logging.getLogger(__name__)
        assert isinstance(text, str)
        result_json = translate(text)
        logger.debug("result_json type: %s", str(result_json))

        self.connection.privmsg(
            nick,
            result_json[0][0][0],
        )

    def default_command(self, connection, nick, command, argument):
        self.connection.notice(
            nick,
            "Not understood: " + command,
        )

    def die(self) -> None:
        raise SrulikDieException


class SrulikDieException(Exception):
    pass


def main() -> None:
    config = {
        'secret_server_password': open('secret_server_password.txt').read().strip(),
        'channel': '#israel',
        'nickname': 'srulik',
        'server': 'chat.freenode.net',
        'port': 6697,
        'translate': {
            'request_headers': {
                n.split(':', 1)[0].strip(): n.split(' ', 1)[1].strip()
                for n in open('translate_request_headers.txt').readlines()
            }
        }
    }


    logger = logging.getLogger(__name__)
    logger.debug('translate_request_headers: %s', translate_request_headers)

    factory = irc.connection.Factory(wrapper=ssl.wrap_socket)

    bot = SrulikBot(
        channel,
        nickname,
        server,
        port,
        connect_factory=factory,
    )
    logger.debug("bot: %s", bot)
    try:
        bot.start()
    except SrulikDieException:
        pass


if __name__ == '__main__':
    main()
